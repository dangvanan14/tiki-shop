package dao;

import org.json.JSONArray;
import repository.ProductRepository;

import javax.inject.Inject;
import java.sql.SQLException;

public class ProductDao {
    private ProductRepository repository;

    @Inject
    public ProductDao(ProductRepository repository) {
        this.repository = repository;
    }

    public JSONArray getAll() throws SQLException {
        return repository.getAll();
    }
}
