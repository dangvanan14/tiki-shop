package dao;

import org.json.JSONArray;
import repository.CartRepository;
import repository.UserRepository;

import javax.inject.Inject;
import java.sql.SQLException;

public class CartDao {
    private CartRepository repository;

    @Inject
    public CartDao(CartRepository repository) {
        this.repository = repository;
    }

    public JSONArray getExist(int product_id, int user_id) throws SQLException {
        return repository.getExist(product_id, user_id);
    }

    public JSONArray getByUser(int user_id) throws SQLException {
        return repository.getByUser(user_id);
    }

    public int removeCart(int cart_id) throws SQLException {
        return repository.removeCart(cart_id);
    }
}
