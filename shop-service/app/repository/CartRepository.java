package repository;

import main.GlobalVariables;
import mapping.Converter;
import models.User;
import org.json.JSONArray;
import play.Logger;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CartRepository {
    private Connection conn;
    private Converter converter;

    @Inject
    public CartRepository(GlobalVariables db, Converter converter) {
        this.conn = db.getConnectionDB();
        this.converter = converter;
    }

    public JSONArray getExist(int product_id, int user_id) throws SQLException {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        JSONArray jsonArray = null;
        try {
            String SQLQuery = "SELECT * FROM cart WHERE product_id = ? AND user_id = ? AND enable = 1";

            preparedStatement = conn.prepareStatement(SQLQuery);
            preparedStatement.setInt(1, product_id);
            preparedStatement.setInt(2, user_id);

            resultSet = preparedStatement.executeQuery();

            resultSet.isFirst();

            jsonArray = converter.convertResultSetIntoJSON(resultSet);
        } catch (Exception e) {
            Logger.error(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
                resultSet = null;
            }
        }
        return jsonArray;
    }

    public JSONArray getByUser(int user_id) throws SQLException {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        JSONArray jsonArray = null;
        try {
            String SQLQuery = "SELECT product.*, count(cart.cart_id) as number FROM cart" +
                    " INNER JOIN product ON product.product_id = cart.product_id " +
                    " WHERE cart.user_id = ? AND cart.enable = 1 GROUP BY product.product_id";

            preparedStatement = conn.prepareStatement(SQLQuery);
            preparedStatement.setInt(1, user_id);

            resultSet = preparedStatement.executeQuery();

            resultSet.isFirst();

            jsonArray = converter.convertResultSetIntoJSON(resultSet);
        } catch (Exception e) {
            Logger.error(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
                resultSet = null;
            }
        }
        return jsonArray;
    }

    public int removeCart(int cart_id) throws SQLException {
        int result = 0;
        PreparedStatement preparedStatement = null;

        try {
            String SQLQuery = "UPDATE cart SET enable = 0 WHERE cart_id = ?";
            preparedStatement = conn.prepareStatement(SQLQuery);

            preparedStatement.setInt(1, cart_id);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            Logger.error(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        return result;
    }
}
