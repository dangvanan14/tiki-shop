package controllers;

import Utils.Compress;
import dao.ProductDao;
import mapping.ResultAPI;
import org.json.JSONArray;
import org.json.JSONObject;
import play.Logger;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;

public class ProductController extends BaseController {
    private ProductDao dao;


    @Inject
    public ProductController(ProductDao dao) {
        this.dao = dao;
    }

    public Result getAll() throws IOException {
        ResultAPI resultAPI = new ResultAPI();
        String result = "";
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = dao.getAll();
            result = resultAPI.jsonString(true, "successful", jsonArray);
        } catch (Exception exception) {
            Logger.error(exception.getMessage());
            result = resultAPI.jsonString(false, "unsuccessful", jsonObject);
        }
        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }
}
