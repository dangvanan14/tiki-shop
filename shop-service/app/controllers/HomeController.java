package controllers;
import play.libs.ws.WSClient;
import play.mvc.*;

import javax.inject.Inject;

public class HomeController extends Controller {
    private WSClient ws;


    @Inject
    public HomeController(){
    }

    public Result index() throws Exception {
        return ok();
    }
}
