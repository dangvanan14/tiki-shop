package controllers;

import Utils.Compress;
import com.fasterxml.jackson.databind.JsonNode;
import dao.CartDao;
import mapping.ResultAPI;
import models.Cart;
import org.json.JSONArray;
import org.json.JSONObject;
import play.Logger;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Date;

//@SubjectPresent
public class CartController extends BaseController {
    private CartDao dao;

    @Inject
    public CartController(CartDao dao) {
        this.dao = dao;
    }

    public Result insert() throws IOException {
        JsonNode json = request().body().asJson();
        String result = "";
        ResultAPI resultAPI = new ResultAPI();
        if (json == null) {
            Logger.error("json body is null");
            result = resultAPI.jsonString(false, "json body is null", new JSONObject());
            setHeaderResponse(result.length());
            return (ok(Compress.compress(result)).as("application/json"));
        }

        try {
            int product_id = json.get("product_id").asInt();
            int user_id = json.get("user_id").asInt();
            final Cart resource = new Cart(user_id, product_id, new Date().getTime(), true);
            resource.insert();
            result = resultAPI.jsonString(true, "successful", new JSONObject());
        } catch (Exception exception) {
            Logger.error(exception.getMessage());
            result = resultAPI.jsonString(false, "unsuccessful", new JSONObject());
        }

        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }

    public Result remove() throws IOException {
        JsonNode json = request().body().asJson();
        String result = "";
        ResultAPI resultAPI = new ResultAPI();
        if (json == null) {
            Logger.error("json body is null");
            result = resultAPI.jsonString(false, "json body is null", new JSONObject());
            setHeaderResponse(result.length());
            return (ok(Compress.compress(result)).as("application/json"));
        }

        try {
            int product_id = json.get("product_id").asInt();
            int user_id = json.get("user_id").asInt();

            //remove first item in list
            JSONArray jsonArray = dao.getExist(product_id, user_id);
            if(jsonArray.length() > 0){
                int remove = dao.removeCart(jsonArray.getJSONObject(0).getInt("cart_id"));
            }
            result = resultAPI.jsonString(true, "successful", new JSONObject());
        } catch (Exception exception) {
            Logger.error(exception.getMessage());
            result = resultAPI.jsonString(false, "unsuccessful", new JSONObject());
        }

        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }

    public Result getByUser(int user_id) throws IOException {
        ResultAPI resultAPI = new ResultAPI();
        String result = "";
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = dao.getByUser(user_id);
            result = resultAPI.jsonString(true, "successful", jsonArray);
        } catch (Exception exception) {
            Logger.error(exception.getMessage());
            result = resultAPI.jsonString(false, "unsuccessful", jsonObject);
        }
        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }
}
