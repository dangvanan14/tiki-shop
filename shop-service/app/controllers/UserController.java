package controllers;

import Utils.Compress;
import com.fasterxml.jackson.databind.JsonNode;
import dao.UserDao;
import mapping.ResultAPI;
import models.User;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import play.Logger;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;

//@SubjectPresent
public class UserController extends BaseController {
    private UserDao dao;

    @Inject
    public UserController(UserDao dao) {
        this.dao = dao;
    }

    public Result insert() throws IOException {
        JsonNode json = request().body().asJson();
        String result = "";
        ResultAPI resultAPI = new ResultAPI();
        if (json == null) {
            Logger.error("user json body is null");
            result = resultAPI.jsonString(false, "user json body is null", new JSONObject());
            setHeaderResponse(result.length());
            return (ok(Compress.compress(result)).as("application/json"));
        }

        try {
            String email = json.get("email").asText();
            String name = json.get("name").asText();
            JSONArray jsonArrayUserId = dao.getByEmail(email);

            //check email is exist
            if (jsonArrayUserId.length() > 0) {
                result = resultAPI.jsonString(false, "Account already exists!", new JSONObject());
            } else {
                String password = json.get("password").asText();
                password = BCrypt.hashpw(password, BCrypt.gensalt());
                final User resource = new User(name, email, password, true);
                resource.insert();
                result = resultAPI.jsonString(true, "successful", new JSONObject());
            }
        } catch (Exception exception) {
            Logger.error(exception.getMessage());
            result = resultAPI.jsonString(false, "unsuccessful", new JSONObject());
        }

        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }

    public Result login() throws IOException {
        JsonNode json = request().body().asJson();
        ResultAPI resultAPI = new ResultAPI();
        String result = "";
        if (json == null) {
            Logger.error("user json body is null");
            result = resultAPI.jsonString(false, "unsuccessful", new JSONObject());
        } else {
            try {
                String email = json.get("email").asText();
                JSONArray jsonArrayUserId = dao.getByEmail(email);

                // check email is exist
                if (jsonArrayUserId.length() == 0) {
                    result = resultAPI.jsonString(false, "Account not exists!", new JSONObject());
                } else {
                    String password = json.get("password").asText();
                    password = BCrypt.hashpw(password, BCrypt.gensalt());

                    JSONObject jsonObject = jsonArrayUserId.getJSONObject(0);
                    if (jsonObject.getString("password").equals(password)) {
                        result = resultAPI.jsonString(false, "Wrong password!", new JSONObject());
                    } else {
                        // put to response
                        JSONObject jsonObjectRes = new JSONObject();
                        jsonObjectRes.put("user_id", jsonObject.get("user_id"));
                        jsonObjectRes.put("email", jsonObject.get("email"));
                        jsonObjectRes.put("name", jsonObject.get("name"));
                        result = resultAPI.jsonString(true, "Successful", jsonObjectRes);
                    }
                }

            } catch (Exception exception) {
                Logger.error(exception.getMessage());
                result = resultAPI.jsonString(false, "unsuccessful", new JSONObject());
            }
        }

        setHeaderResponse(result.length());
        return (ok(Compress.compress(result)).as("application/json"));
    }
}
