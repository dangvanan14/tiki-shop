package models;

import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Cart extends Model {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int cart_id;
    private int user_id;
    private int product_id;
    private Long create_at;
    private boolean enable;

    public Cart(int user_id, int product_id, Long create_at, boolean enable) {
        this.user_id = user_id;
        this.product_id = product_id;
        this.create_at = create_at;
        this.enable = enable;
    }

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Long getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Long create_at) {
        this.create_at = create_at;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
