package models;

import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product extends Model {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int product_id;
    private String name;
    private String image;
    private float price;
    private int categories;
    private int enable;

    public Product(int product_id, String name, String image, float price, int categories, int enable) {
        this.product_id = product_id;
        this.name = name;
        this.image = image;
        this.price = price;
        this.categories = categories;
        this.enable = enable;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getCategories() {
        return categories;
    }

    public void setCategories(int categories) {
        this.categories = categories;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
