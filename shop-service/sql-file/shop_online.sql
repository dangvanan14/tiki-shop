-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 02, 2020 lúc 06:10 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shop_online`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `create_at` bigint(20) NOT NULL,
  `enable` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `product_id`, `create_at`, `enable`) VALUES
(1, 2, 1, 1577774631980, 0),
(2, 2, 1, 1577776727127, 0),
(3, 2, 6, 1577776731535, 0),
(4, 2, 1, 1577777935824, 1),
(5, 2, 1, 1577778838209, 1),
(6, 1, 1, 1577941349812, 1),
(7, 1, 5, 1577941538918, 0),
(8, 1, 3, 1577941611971, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `categories` int(11) NOT NULL,
  `enable` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`product_id`, `name`, `image`, `price`, `categories`, `enable`) VALUES
(1, 'Apple', 'https://www.delibunny.com/wp-content/uploads/2019/02/apple.jpeg', 4.95, 1, 1),
(2, 'Orange', 'https://img1.exportersindia.com/product_images/bc-full/2019/7/6458641/fresh-orange-1564033295-5014658.jpeg', 3.99, 1, 1),
(3, 'Custard Apple', 'https://plantogram.com/wa-data/public/shop/products/24/04/424/images/1116/1116.970.jpg', 5.1, 1, 1),
(4, 'Papaya', 'https://mk0mexiconewsdam2uje.kinstacdn.com/wp-content/uploads/2019/10/papapya.jpg', 2.6, 1, 1),
(5, 'Tamarind', 'https://chowhound1.cbsistatic.com/thumbnail/800/0/chowhound1.cbsistatic.com/blog-media/2018/05/how-to-use-tamarind-chowhound-670x454.jpg', 5.3, 1, 1),
(6, 'Avocado', 'https://www.cedars-sinai.org/content/cedars-sinai/blog/healthy-and-delicious-avocado/_jcr_content/par/contentblock/customparsys/image/image.img.jpg/1570745419293.jpg', 2.45, 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `enable` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`user_id`, `email`, `name`, `password`, `enable`) VALUES
(1, 'dangvanan14@gmail.com', 'Đặng Văn An', '$2a$10$ilA6sDWeMx0kJOSAAJ9DVu1drE26NcWn.d8Vbq/rk.gHDv039kVHq', 1),
(2, 'dangvanan15@gmail.com', 'Văn An', '$2a$10$jj89w6dyAE6ZjKOqJ.XGeuDs.NNv8gXAxX9sJ3aUzm4zwh/IjlORy', 1),
(3, 'dangvanan16@gmail.com', 'Dang Van B', '$2a$10$oZ/9Osb0GFLDFwdpsjuzIO8Fi9lVDsG31G2cbeM0mVUz2SX3MTCVC', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
