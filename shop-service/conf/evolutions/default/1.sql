# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cart (
  cart_id                       integer auto_increment not null,
  user_id                       integer not null,
  product_id                    integer not null,
  create_at                     bigint,
  enable                        tinyint(1) default 0 not null,
  constraint pk_cart primary key (cart_id)
);

create table product (
  product_id                    integer auto_increment not null,
  name                          varchar(255),
  image                         varchar(255),
  price                         float not null,
  categories                    integer not null,
  enable                        integer not null,
  constraint pk_product primary key (product_id)
);

create table user (
  user_id                       integer auto_increment not null,
  name                          varchar(255),
  email                         varchar(255),
  password                      varchar(255),
  enable                        tinyint(1) default 0 not null,
  constraint pk_user primary key (user_id)
);


# --- !Downs

drop table if exists cart;

drop table if exists product;

drop table if exists user;

