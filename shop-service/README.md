# Shop Service

# Sub-Modules Organization

The project is splitted in several sub-modules:

* **app**:
Source code
* **conf**:
Configuration
* **sql-file**:
MySQL file
* **project**:
Sbt plugin configuration.
* **public**:
Public folder with html, css, static files
* **sbt-dist**:
Services and framework related to WebEngine
* **test**:
Test module

# Building

Open Shop Service in [Intellij](https://www.jetbrains.com/idea/)

## Requirements

Running the Shop Service requires Java 8.
Depending on the features you want to use, you may need some third-party software, such as Libre Office and pdftohtml for document preview or ImageMagick for pictures. The list of third-party software is available in our Admin documentation: [Installing and Setting Up Related Software](https://gitlab.com/dangvanan14/tiki-shop/blob/master/shop-service/README.md).

Building the Nuxeo Platform requires the following tools:

* JDK 8 (Oracle's JDK or OpenJDK recommended)
* Sbt 0.13.18
* Git (obviously)

# Deploying

1. Get the source code:
```
git clone git@gitlab.com:dangvanan14/tiki-shop.git
cd shop-service
```
2. Build using Sbt:
```
sbt run
```

# Licensing

You are welcome to use this however you wish within the MIT license.

