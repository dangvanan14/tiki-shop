var server_url = '';
$(document).ready(function () {
    $.getJSON("./env.json", function (data) {
        server_url = data.server_url;
        getCart();
    });

    checkUser();
    $('#home_page').click(function (e) {
        location.href = "./index.html"
    });
});

function getCart(){
    fetch( server_url + "/cart/getbyuser?user_id=" + getCookie("user_id"))
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            let total_price = 0;
            if (json.success === true) {
                console.log(json);
                console.log(json.data);
                if (json.data.length > 0) {
                    let childs = '<div class="col-lg-12"><h3>Danh sách giỏ hàng</h3></div> ';
                    for (let i = 0; i < json.data.length; i++) {
                        total_price += json.data[i].price * json.data[i].number;
                        let child = '<div class="col-lg-4">\n' +
                            '<div class="container-video">\n' +
                            '                                <img src="' + json.data[i].image + '"\n' +
                            '                                        frameborder="0" allowfullscreen class="video"></img>\n' +
                            '                            </div>' +
                            '                                    <h4>' + json.data[i].number + ' x ' + json.data[i].name + '</h4>\n' +
                            '                                    <p>' + json.data[i].price.toLocaleString() + ' USD</p>\n' +
                            '                                    <div class="heart white">\n' +
                            '                                        <p> <i style="cursor: pointer" class="fa fa-remove" id="product_id_' + json.data[i].product_id + '" onclick="removeCart(this)"></i></p>\n' +
                            '                                    </div>\n' +
                            '                                </div>';
                        childs += child;
                    }
                    document.getElementById('list_product').innerHTML = childs;
                    $('#total_price').html('<strong>Tổng tiền: ' + total_price.toLocaleString() + ' USD</strong>')
                }
            }
        })
        .catch(function (ex) {
            console.error("parsing failed", ex);
        });
}

function removeCart(e) {
    let id = e.getAttribute("id").split("_");
    product_id = id[2];

    let email = getCookie("email");
    if (email == "") {
        $('#show-login').show();
    }else {
        let data = {
            product_id: product_id,
            user_id: getCookie("user_id"),
            action: 0
        };

        fetch(server_url + "/cart/remove", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.success === true) {
                    location.reload();
                } else {
                    alert("Thêm vào giỏ hàng thất bại!");
                }
            })
            .catch(function (ex) {
                console.error("parsing failed", ex);
            });
    }
}

function checkUser() {
    let email = getCookie("email");
    if (email != "") {
        $('#name_login').show();
        $('#name_login').text(getCookie("name"));
        $('#show-login').hide();
        $('#show-singin').hide();
        $('#logout').show();
        $('#cart').show();
    }else {
        location.href="./index.html";
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
